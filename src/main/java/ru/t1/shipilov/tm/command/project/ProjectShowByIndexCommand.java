package ru.t1.shipilov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shipilov.tm.model.Project;
import ru.t1.shipilov.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private final String NAME = "project-show-by-index";

    @NotNull
    private final String DESCRIPTION = "Show project by index.";

    @Override
    public void execute() {
        @NotNull final String userId = getUserId();
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final Project project = getProjectService().findOneByIndex(userId, index);
        showProject(project);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
