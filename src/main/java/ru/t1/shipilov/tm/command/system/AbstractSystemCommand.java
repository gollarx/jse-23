package ru.t1.shipilov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shipilov.tm.api.service.ICommandService;
import ru.t1.shipilov.tm.command.AbstractCommand;
import ru.t1.shipilov.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
